# Game of Three

A simple two player game developed using Laravel, Websockets and VueJs

#### Requirements:
 - PHP 7.2+ (must have it)
 - Composer ([installation guide](https://getcomposer.org/doc/00-intro.md))
 - MySQL database (optional)
 - Laravel Homestead (optional - not recommended) ([installation guide](https://laravel.com/docs/5.7/homestead))

## Steps to get it working method 1 (easiest - I can win):
**For this method you only need `PHP` because free mysql server is used which is set into the `.env` (config) file and all database migrations were already called**

1. Clone this repository
2. `cd ./game-of-three` (Get into the project directory)
3. `composer install` (Pull and install project dependencies)
4. `cp .env.example .env` (Copy .env.example to .env)
5. `php artisan key:generate` (Not actually needed since i provided it already into the .env.example file)
6.  Open the game-of-three directory in new terminal window and call the following command: `php artisan serve` (which will start Built-in webserver and you will have to open the given address in browser )
7.  Open the game-of-three directory in new terminal window and call the following command: `php artisan serve:games` (this command will start the websocket server which is needed for communication between the server and clients)

## Steps to get it working method 2 (medium - Bring It On):
**For this method you will need `PHP` and running `MySQL` server**

1. Clone this repository
2. `cd ./game-of-three` (Get into the projects directory)
3. Run `composer install` (Pull and install project dependencies)
4. `cp .env.example .env` (Copy .env.example to .env)
5. Edit `.env` file and set/change database variables to your corresponding MySQL server (DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD)
6. Run `php artisan migrate` (To run database migrations)
7. Open the game-of-three directory in new terminal window and call the following command: `php artisan serve` (which will start Built-in webserver and you will have to open the given address in browser )
8. Open the game-of-three directory in new terminal window and call the following command: `php artisan serve:games` (this command will start the websocket server which is needed for communication between the server and clients)

## Steps to get it working method 3 (Hardcore):
**For this method you will need `PHP` and `Sqlite` driver installed.**

1. Clone this repository
2. `cd ./game-of-three` (Get into the projects directory)
3. Run `composer install` (Pull and install project dependencies)
4. `cp .env.example .env` (Copy .env.example to .env)
5. Edit `.env` file and delete `#DB CONFIG FOR MYSQL section` and uncomment the `#DB CONFIG FOR SQLITE section` (everything in-between the section tags)
6. Run `php artisan migrate` (To run database migrations)
7. Open the game-of-three directory in new terminal window and call the following command: `php artisan serve` (which will start Built-in webserver and you will have to open the given address in browser )
8. Open the game-of-three directory in new terminal window and call the following command: `php artisan serve:games` (this command will start the websocket server which is needed for communication between the server and clients)

## Steps to get it working method 4 (Insane):
1. Clone this repository
2. Setup the project using [Laravel Homestead](https://laravel.com/docs/5.7/homestead) (All the needed steps are given previously)
3. Repeat the steps from method 2

## How to play the game:

1. Register with email and password
2. Login
3. Click the play button and wait for an opponent
4. Enjoy
5. Play again

## Developed by:

- [Slobodan Tonchevski](https://www.linkedin.com/in/slobodan-tonchevski-53b74310b/)

## Contact email:

- [internalle@gmai.com](mailto:internalle@gmai.com)

