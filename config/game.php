<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Game config file
    |--------------------------------------------------------------------------
    | Here are going to be stored all game necessary config variables
    |
    */

    'select_options' => [-1, 0, 1],
];
