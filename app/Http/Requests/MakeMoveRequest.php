<?php

namespace App\Http\Requests;

use App\Services\GameService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MakeMoveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param GameService $game_service
     *
     * @return bool
     */
    public function authorize(GameService $game_service)
    {
        return $game_service->userInGame($this->user()['id'], $this->get('game_id') ?? 0);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'game_id' => 'required|int',
            'selected_option' => ['required', 'int', Rule::in(config('game.select_options'))]
        ];
    }

    public function data()
    {
        return $this->all('game_id', 'selected_option');
    }
}
