<?php

namespace App\Http\Controllers;

use App\Events\PlayerJoined;
use App\Exceptions\NumberNotDivisableException;
use App\Http\Requests\MakeMoveRequest;
use App\Http\Requests\StartGameRequest;
use App\Services\GameService;

class GamesController extends Controller
{
    /**
     * @var GameService
     */
    protected $game_service;

    /**
     * GamesController constructor.
     *
     * @param GameService $game_service Game service injection
     */
    public function __construct(GameService $game_service) {
        $this->middleware('auth');

        $this->game_service = $game_service;
    }

    /**
     * Play game
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function join()
    {
        try {
            $data = $this->game_service->prepareGameData();
        } catch (\Exception $e) {
            \Log::error($e);

            return response([
                'message' => 'Woops.Technical difficulties.',
                'success' => false
            ], 500);
        }

        return response($data);
    }


    /**
     * Start a game
     *
     * @param StartGameRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function start(StartGameRequest $request)
    {
        try {
            $this->game_service->startGame($request->data());
        } catch (\Exception $e) {
            \Log::error($e);

            return response([
                'message' => 'Woops.Technical difficulties.',
                'success' => false
            ], 500);
        }

        return response([
            'success' => true,
        ]);
    }

    /**
     * Called when player choose an option
     *
     * @param MakeMoveRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function makeMove(MakeMoveRequest $request)
    {
        try {
            $this->game_service->handlePlayerMove($request->data());
        } catch (NumberNotDivisableException $e) {
            return response([
                'message' => $e->getMessage(),
                'success' => false
            ]);
        }
        catch (\Exception $e) {
            \Log::error($e);

            return response([
                'message' => 'Woops. Sorry',
                'success' => false
            ], 500);
        }

        return response([
            'success' => true
        ]);
    }
}
