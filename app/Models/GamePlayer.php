<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GamePlayer extends Model
{
    protected $fillable = ['player_id', 'game_id'];
    /**
     * Relations
     */

    public function game()
    {
        return $this->belongsTo(Game::class);
    }
}
