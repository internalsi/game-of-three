<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable = ['status'];


    /**
     * Relations
     */

    public function players()
    {
        return $this->hasMany(GamePlayer::class, 'game_id', 'id');
    }

    public function player_moves()
    {
        return $this->hasMany(GamePlayerMove::class, 'game_id', 'id');
    }
}
