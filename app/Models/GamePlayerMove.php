<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GamePlayerMove extends Model
{
    protected $fillable = [
        'game_id',
        'game_player_id',
        'selected_option'
    ];
}
