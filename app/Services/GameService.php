<?php
namespace App\Services;

use App\Events\GameFinished;
use App\Events\GameStarted;
use App\Events\NextOptionSelected;
use App\Exceptions\MoveNotAllowedException;
use App\Exceptions\NumberNotDivisableException;
use App\Repositories\GamePlayerMoveRepository;
use App\Repositories\GamePlayerRepository;
use App\Repositories\GameRepository;

class GameService
{
    /**
     * @var GameRepository
     */
    protected $game_repository;

    /**
     * @var GamePlayerRepository
     */
    protected $game_player_repository;

    /**
     * @var GamePlayerMoveRepository
     */
    protected $game_player_move_repository;

    /**
     * GameService constructor.
     *
     * @param GameRepository $game_repository
     * @param GamePlayerRepository $game_player_repository
     * @param GamePlayerMoveRepository $game_player_move_repository
     */
    public function __construct(
        GameRepository $game_repository,
        GamePlayerRepository $game_player_repository,
        GamePlayerMoveRepository $game_player_move_repository
    ) {
        $this->game_repository = $game_repository;
        $this->game_player_repository = $game_player_repository;
        $this->game_player_move_repository = $game_player_move_repository;
    }

    /**
     * Prepare game data so the logged user can join a game.
     *
     * @return array
     */
    public function prepareGameData(): array
    {
        $channel = 'game.';
        $user = auth()->user();

        $game = $this->game_repository->getFirstPlayerGameByStatus($user['id'], 'PENDING');

        if (empty($game)) {
            $game = $this->game_repository->getGameByStatusAndNumberOfPlayers(1, 'PENDING');

            if (empty($game)) {
                $game = $this->game_repository->create();
            }

            $this->game_player_repository->create([
                'game_id' => $game['id'],
                'player_id' => $user['id']
            ]);
        }

        $channel .= $game['id'];

        return [
            'channel' => $channel,
            'game_data' => $game
        ];
    }

    /**
     * Start a game
     *
     * @param array $request_data
     *
     * @return void
     */
    public function startGame(array $request_data): void
    {
        $user = auth()->user();
        $number = rand(2 , 1000);
        $game = $this->game_repository->findOrFail($request_data['game_id']);

        $this->game_repository->update(['status' => "STARTED"], $game['id']);

        $this->game_player_move_repository->create([
            'game_id' => $game['id'],
            'game_player_id' => $user['id'],
            'selected_option' => $number
        ]);

        $data = [
            'game' => $game->toArray(),
            'player' => $user,
            'selected_number' => $number,
            'player_options' => config('game.select_options')
        ];

        broadcast(new GameStarted($user, $data));
    }

    /**
     * Handle player move.
     *
     * @param array $request_data
     *
     * @throws NumberNotDivisableException
     * @throws MoveNotAllowedException
     */
    public function handlePlayerMove(array  $request_data): void
    {
        $user = auth()->user();
        $game = $this->game_repository->getGameWithLastPlayerMove($request_data['game_id']);

        $last_move = $game['player_moves'][0]->toArray();
        $selected_option = abs((int)$request_data['selected_option']);

        if (empty($last_move) || ($last_move['game_player_id'] === $user['id'])) {
            throw new MoveNotAllowedException('Sorry you are now allowed to make that move.');
        }

        if ($request_data['selected_option'] >= 0) {
            $number = (int)$last_move['selected_option'] + $selected_option;
        } else {
            $number = (int)$last_move['selected_option'] - $selected_option;
        }

        if (($number % 3) === 0) {
            $number = $number / 3;

            $this->game_player_move_repository->create([
                'game_id' => $game['id'],
                'game_player_id' => $user['id'],
                'selected_option' => $number
            ]);

            $data = [
                'game' => $game->toArray(),
                'player' => $user->toArray(),
                'selected_number' => $number,
                'player_options' => config('game.select_options')
            ];

            broadcast(new NextOptionSelected($user, $data));

            if ($number === 1) {
                $this->game_repository->update(['status' => 'FINISHED'], $game['id']);

                broadcast(new GameFinished($user, $data));
            }

            return;
        }

        throw new NumberNotDivisableException('The number ' . $number . ' is not divisible with  3');
    }

    /**
     * Check if logged user participate in a game
     *
     * @param integer $user_id
     * @param integer $game_id
     *
     * @return boolean
     */
    public function userInGame(int $user_id, int $game_id): bool
    {
        return !!$this->game_player_repository->getGamePlayerByGameId($user_id, $game_id);
    }

    /**
     * Approve if the player is allowed to make the move
     *
     * @param int $user_id
     * @param int $game_id
     *
     * @return boolean
     */
    public function approvePlayerMove(int $user_id, int $game_id): bool
    {
        //
    }
}
