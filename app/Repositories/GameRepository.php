<?php
namespace App\Repositories;

use App\Models\Game;
use Illuminate\Database\Eloquent\Model;

class GameRepository extends AbstractEloquentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Game::class;
    }

    /**
     * Get first player game by status
     *
     * @param int $user_id
     * @param string $game_status
     *
     * @return mixed
     */
    public function getFirstPlayerGameByStatus(int $user_id, string $game_status)
    {
        return $this->model->whereHas('players', function ($q) use ($user_id) {
            $q->where('player_id', $user_id);
        })->with('players')
            ->where('status', $game_status)
            ->first();
    }

    /**
     * Get first game that has less then 2 players
     *
     * @param integer $number_of_players
     * @param string $status
     *
     * @return Model
     */
    public function getGameByStatusAndNumberOfPlayers(int $number_of_players, string $status)
    {
        return $this->model->has('players', '=', $number_of_players)
            ->with('players')
            ->where('status', $status)
            ->first();
    }


    /**
     * Create new game for given player
     *
     * @param int $user_id
     *
     * @return mixed
     */
    public function createNewGameForPlayer(int $user_id)
    {
        $game = $this->create([]);

        $game->players()->create([
            'player_id' => $user_id
        ]);

        return $game;
    }

    /**
     * Get game with last player move
     *
     * @param int $game_id
     *
     * @return Model
     */
    public function getGameWithLastPlayerMove(int $game_id)
    {
        return $this->model->with(['player_moves' => function($q) {
            $q->orderBy('id', 'DESC')
                ->first();
        }])->find($game_id);
    }

}
