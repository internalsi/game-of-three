<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;

/**
 * Class AbstractEloquentRepository
 *
 * @package App\Repositories
 */
abstract class AbstractEloquentRepository implements RepositoryInterface
{
    /**
     * Laravel's service container.
     *
     * @var App
     */
    private $app;

    /**
     * Eloquent Model.
     *
     * @var Model
     */
    protected $model;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    abstract public function model();

    /**
     * Resolve the model from the Service Container.
     *
     * @throws Exception
     *
     * @return mixed
     */
    private function makeModel()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new Exception("Class " . $this->model() . " must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }

    /**
     * Fetch all records from the database
     *
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all($columns = ['*'])
    {
        return $this->model->get($columns);
    }

    /**
     * Create a new model instance and save to database.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function create($data = [])
    {
        return $this->model->create($data);
    }

    /**
     * Insert new row/s into this model's table.
     *
     * @param array $data Data array or array of data arrays
     *
     * @return boolean
     */
    public function insert(array $data)
    {
        return $this->model->insert($data);
    }

    /**
     * Update the database records.
     *
     * @param array $data
     * @param mixed $attribute
     * @param string $column
     *
     * @return mixed
     */
    public function update($data, $attribute, $column = 'id')
    {
        if (!empty($data)) {
            $data = $this->onlyFillableFields($data);
        }

        if (is_array($attribute)) {
            return $this->model->whereIn($column, $attribute)->update($data);
        }

        return $this->model->where($column, '=', $attribute)->update($data);
    }

    /**
     * Delete the database records.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }

    /**
     * Find the resource by id.
     *
     * @param mixed $id
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function find($id, $columns = ['*'])
    {
        return $this->model->find($id, $columns);
    }

    /**
     * Find or fail function for the resource by id.
     *
     * @param mixed $id
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findOrFail($id, $columns = ['*'])
    {
        return $this->model->findOrFail($id, $columns);
    }

    /**
     * Find the resource by any given attribute.
     *
     * @param mixed $attribute
     * @param mixed $value
     * @param array $columns
     *
     * @return
     */
    public function findBy($attribute, $value, $columns = ['*'])
    {
        return $this->model->where($attribute, $value)->get($columns);
    }

    /**
     * Find the first resource by any given attribute.
     *
     * @param mixed $attribute
     * @param mixed $value
     * @param array $columns
     *
     * @return mixed
     */
    public function findFirstBy($attribute, $value, $columns = ['*'])
    {
        return $this->model->where($attribute, $value)->firstOrFail($columns);
    }

    /**
     * Return an instance of query builder for the resource by any given attribute.
     *
     * @param mixed $attribute
     * @param mixed $value
     * @param string $operator
     *
     * @return mixed
     */
    public function where($attribute, $value, $operator = '=')
    {
        return $this->model->where($attribute, $operator, $value);
    }

    /**
     * Filter the given data through the model's fillable fields
     *
     * @param array $data
     *
     * @return array
     */
    protected function onlyFillableFields($data)
    {
        $data = collect($data);
        $data_keys = $data->keys();
        $fillable_fields = collect($this->model->getFillable());

        $diff = $data_keys->diff($fillable_fields)->values();

        if ($diff->isNotEmpty()) {
            \Log::warning('Trying to create/update these non fillable fields ('. implode(', ', $diff->toArray()) . ') on ' . $this->model() . ' model.');
        }

        if ($fillable_fields->isEmpty()) {
            \Log::warning($this->model() . ' model has no fillable fields but his repository methods for create/update are being called.');

            return [];
        }

        return $data->filter(function ($value, $key) use ($fillable_fields) {
                return $fillable_fields->contains($key);
            })->all();
    }
}
