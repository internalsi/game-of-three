<?php

namespace App\Repositories\Interfaces;

/**
 * Interface RepositoryInterface
 */
interface RepositoryInterface
{
    public function all($columns = ['*']);

    public function create($data);

    public function insert(array $data);

    public function update($data, $attribute, $field = 'id');

    public function delete($id);

    public function find($id, $columns = ['*']);

    public function findBy($field, $value, $columns = ['*']);

    public function where($field, $value, $operator = '=');
}
