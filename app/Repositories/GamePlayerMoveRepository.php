<?php
namespace App\Repositories;

use App\Models\GamePlayerMove;

class GamePlayerMoveRepository extends AbstractEloquentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return GamePlayerMove::class;
    }

}
