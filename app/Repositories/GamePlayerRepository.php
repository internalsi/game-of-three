<?php
namespace App\Repositories;

use App\Models\GamePlayer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class GamePlayerRepository extends AbstractEloquentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return GamePlayer::class;
    }

    /**
     * Get player by game id
     *
     * @param integer $player_id
     * @param integer $game_id
     *
     * @return mixed
     */
    public function getGamePlayerByGameId(int $player_id, int $game_id)
    {
        $result = $this->model
            ->where('game_id', $game_id)
            ->where('player_id', $player_id)
            ->first();

        return $result;
    }
}
