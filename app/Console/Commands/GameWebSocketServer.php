<?php

namespace App\Console\Commands;

use BeyondCode\LaravelWebSockets\Console\StartWebSocketServer;

class GameWebSocketServer extends StartWebSocketServer
{
    protected $signature = 'serve:games {--host=0.0.0.0} {--port=6001} {--debug : Forces the loggers to be enabled and thereby overriding the app.debug config setting } ';

    protected $description = 'Start the games websocket server';
}
