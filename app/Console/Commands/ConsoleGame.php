<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use React\EventLoop\Factory as LoopFactory;

class ConsoleGame extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $loop = LoopFactory::create();

        $loop->addReadStream(STDIN, function ($stream) use ($loop) {
            $chunk = fread($stream, 64 * 1024);
            // reading nothing means we reached EOF
            if ($chunk === '') {
                $loop->removeReadStream($stream);
                stream_set_blocking($stream, true);
                fclose($stream);
                return;
            }
            //TODO: Implement console version
        });

        $loop->run();
    }
}
